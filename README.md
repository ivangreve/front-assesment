# Front-assesment

## Libraries Used and Why??:
- material-ui : A Simple way to make an UI quickly and nice!
- react-axios: Although in this project I could use a simple fetch(), I decided to use axios because this lib handle me the success and error request and I love it :D.
- react-redux: To use a global store and reducers.

## Getting Started
### Prerequisites
- Install npm dependencies.
    - Run: `npm install`

### Running
- In the project root directory run:
    - `npm start` (Runs the app in the development mode.)
- Open [http://localhost:3000](http://localhost:3000) to show the app.


### Test Runner
- In the project root directory run:
    - `npm test`


### Build App
- In the project root directory run:
    - `npm run build`
