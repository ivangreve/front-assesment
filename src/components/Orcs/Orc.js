import React, { Component } from 'react'
import OrcInfo from "../OrcInfo/index"
import ImgNodiponible from "../../media/images/nodisponible.jpg"


export default class Orc extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
  };
    this.handleClickOpenDialog = this.handleClickOpenDialog.bind(this)
    this.handleClickCloseDialog = this.handleClickCloseDialog.bind(this);
  }


  handleClickOpenDialog = () => {
    this.setState({ open: true });
  };

  handleClickCloseDialog = () => {
    this.setState({ open: false });
  };

    render() {
        return (
            <div className="orc">
              <div className="orc-image">
                <img
                src= {this.props.orcData.thumbnail}
                alt={this.props.orcData.name}

                onError={(e) => {
                    e.target.src = ImgNodiponible;   
                }}
                />
              </div>

              <h4 className="orc-name">{this.props.orcData.name}</h4>
              <p className="orc-name">Age: {this.props.orcData.age}</p>
              
              <div className="info-action">
                <button
                  type="button"
                  onClick={this.handleClickOpenDialog}
                >
                  More Info
                </button>
              </div>
              <OrcInfo
                onClickClose={this.handleClickCloseDialog}
                open={this.state.open}

                orcName={this.props.orcData.name}
                orcAge={this.props.orcData.age}
                orcFriendsArray={this.props.orcData.friends}
                orcHairColor={this.props.orcData.hair_color}
                orcHeight={this.props.orcData.height}
                orcProfessions={this.props.orcData.professions}
                orcWeight={this.props.orcData.weight}
              />  
            </div>
          );
    }
}

