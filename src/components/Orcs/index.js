import React, { Component } from 'react';
import { connect } from 'react-redux'
import axios from "axios";
import Grid from '@material-ui/core/Grid';
import Orc from "./Orc"
import saveOrcsDataAction from '../../redux/actions/orcs/saveOrcsData';

class Orcs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            page: 1,
            pageLenght: 20,
        }
        this.generateOrcsComponents = this.generateOrcsComponents.bind(this);
    }

    getOrcsData() {
        let url =`https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json`;
        this.setIsLoading();
        axios.get(url).then(response => {
            this.props.saveOrcsDataAction(response.data.Brastlewark);

        });
    }

    setIsLoading(){
        this.setState({
            isLoading: true,
        });
    }

    componentDidMount() {
        this.getOrcsData();
    }


    generateOrcsComponents(){
        let orcsData = this.props.orcsReducer._filteredData;
        let orcsComponent = orcsData
        .map((orc) => {
            return (
                <Orc item orcData={orc} key={orc.id} />

                // <Grid item key={orc.id}  >
                // </Grid>
            )
        });

      return orcsComponent;
    }

    render() {
        return (
            <div className="orcs-wrapper">
                <Grid container className="orcs">
                    { this.generateOrcsComponents() }
                </Grid>
            </div>

        );
    }
}



const mapStateToProps = (state) => {
    return{
        orcsReducer: state.orcsReducer,
    }
};

const mapDispatchToProps = {
    saveOrcsDataAction
};

export default connect(mapStateToProps, mapDispatchToProps)(Orcs);


