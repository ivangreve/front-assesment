import React, { Component } from "react";
import { connect } from 'react-redux'
import PropTypes from 'prop-types';
import ImgClient from "../../media/images/client-green.jpg"
import ImgSearchGreen from "../../media/icons/search-green.png"
import IconBack from "../../media/icons/back.png"
import filterOrcsAction from "../../redux/actions/orcs/filterOrcs"

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCart: false,
      mobileSearch: false
    };

    this.handleSearch = this.handleSearch.bind(this);
  }


  handleMobileSearch(e) {
    e.preventDefault();
    this.setState({
      mobileSearch: true
    });
  }
  handleSearchNav(e) {
    e.preventDefault();
    this.setState(
      {
        mobileSearch: false
      },
      function() {
        this.refs.searchBox.value = "";
        //this.props.handleMobileSearch();
      }
    );
  }

  handleSearch(e){
    debugger;
    this.props.filterOrcsAction(e.target.value, "");
  }


  render() {

    return (
      <header>
        <div className="container">
          <div>

            <img alt="" 
              className="logo" 
              style={{width:"40px", verticalAlign:"middle",marginRight:"5px"}} 
              src={ImgClient}/>
            <span className="clientname">{this.props.title}</span>


          </div>

          <div className="search">
          
           {/* <div>Cant.: <strong>{this.props.totalItems}</strong>  Sub Total: <strong>{this.props.total}</strong></div> */}
            <a
              className="mobile-search"
              href="# "
              //onClick={this.handleMobileSearch.bind(this)}
             
            >
           
              <img
                src={ImgSearchGreen}
                alt="search"
              />
            </a>
            <form
              action="#"
              method="get"
              className={
                this.state.mobileSearch ? "search-form active" : "search-form"
              }
            >
              <a
                className="back-button"
                href="# "
                onClick={this.handleSearchNav.bind(this)}
              >
                <img
                  src={IconBack}
                  alt="back"
                />
              </a>
              
              <input
                type="search"
                ref="searchBox"
                placeholder="Search Orc"
                className="search-keyword"
                onChange={this.handleSearch}
                
              />
              <button
                className="search-button"
                type="submit"
                //onClick={this.handleSubmit.bind(this)}
              />
            </form>
          </div>

        </div>
      </header>
    );
  }
}

Header.propTypes = {
  title: PropTypes.string,
};


//Conecto el store de la applicacion y lo asocio a las props del componente
const mapStateToProps = (state) => {
  return{
      orcsReducer: state.orcsReducer,
  }
};

const mapDispatchToProps = {
  filterOrcsAction
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
