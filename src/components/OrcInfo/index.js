import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';


export default class OrcInfo extends Component {
    
  render() {
    const styles = theme => ({
        root: {
          margin: 0,
          padding: 8,
          minWidth:500
        },
        closeButton: {
          position: 'absolute',
          right: 8,
          top: 8,
          color: theme.palette.grey[500],
        },
      });

    const DialogTitle = withStyles(styles)(props => {
        const { children, classes, onClose } = props;
        return (
          <MuiDialogTitle disableTypography className={classes.root}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
              <IconButton aria-label="Close" className={classes.closeButton} onClick={onClose}>
                <CloseIcon />
              </IconButton>
            ) : null}
          </MuiDialogTitle>
        );
      });

    return (
      <div>

        <Dialog
            aria-labelledby="customized-dialog-title"
            open={this.props.open}
        >
        <DialogTitle id="customized-dialog-title" onClose={this.props.onClickClose}>
        {this.props.orcName}
        </DialogTitle>
        <Divider />
        <List>
          <ListItem >
            <ListItemText primary="Age" secondary={this.props.orcAge} />
          </ListItem>
          <ListItem >
            <ListItemText primary="Height" secondary={this.props.orcHeight} />
          </ListItem>
          <ListItem >
            <ListItemText primary="Weight" secondary={this.props.orcWeight} />
          </ListItem>
          <ListItem >
            <ListItemText primary="Hair Color" secondary={this.props.orcHairColor} />
          </ListItem>
          <ListItem >
            <ListItemText primary="Friends" secondary={this.props.orcFriendsArray.join(',')} />
          </ListItem>
          <ListItem >
            <ListItemText primary="Professions" secondary={this.props.orcProfessions.join(',')} />
          </ListItem>
        </List>

        </Dialog>
      </div>
    )
  }
}

OrcInfo.propTypes = {
    orcName: PropTypes.string,
    orcAge: PropTypes.number,
    orcHeight: PropTypes.number,
    orcWeight: PropTypes.number,
    orcHairColor: PropTypes.string,
    orcFriendsArray: PropTypes.array,
    orcProfessions: PropTypes.array,
};


