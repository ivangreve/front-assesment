import { type as saveOrcsDataType } from '../actions/orcs/saveOrcsData';
import { type as filterOrcsType } from '../actions/orcs/filterOrcs';



const defaultState = {
        _orcsData: [],
        _filteredData: [],
    };

function reducer(state = defaultState, { type, payload }){
    switch (type) {
        case saveOrcsDataType: {
            return saveOrcsData(payload.data);
        }
        case filterOrcsType: {
            return filterOrcs(payload.nameFilter, payload.ageFilter);
        }
        default:
            return state;
    }

    function saveOrcsData(data){
        return {
            ...state,
            _orcsData: data,
            _filteredData: data,
        };
    }
    
    function filterOrcs(nameFilter, ageFilter){
        const result = state._orcsData.filter(orc => orc.name.toLowerCase().includes(nameFilter.toLowerCase()));
        return {
            ...state,
            _filteredData: result,
        };
    }

}

export default reducer;