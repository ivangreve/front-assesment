export const type = 'FILTER_ORCS';

const filterOrcs = (nameFilter, ageFilter) =>{
    return{
        type: type,
        payload: {
            nameFilter,
            ageFilter,
        },
    };
};


export default filterOrcs;