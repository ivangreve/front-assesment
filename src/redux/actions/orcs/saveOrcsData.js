export const type = 'SAVE_ORCS_DATA';

const saveOrcsData = (data) =>{
    return{
        type: type,
        payload: {
            data,
        },
    };
};


export default saveOrcsData;