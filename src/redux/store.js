import { createStore, combineReducers } from 'redux';
import orcsReducer from './reducers/orcsReducer';


const reducers = combineReducers({
    orcsReducer,
});
const store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;