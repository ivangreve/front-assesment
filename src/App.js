
import React, { Component } from 'react';
import Header from './components/Header';
import Orcs from './components/Orcs';
import "./scss/style.scss";


class App extends Component {
  render() {
    return (
      <div className="mdc-typography">
        <Header title={"Welcome Hero"}/> 
        <Orcs/>
      </div>
    );
  }
}

export default App;